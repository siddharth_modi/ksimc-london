//
//  NotificationSubScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 14/12/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "NotificationSubScreen.h"


@interface NotificationSubScreen ()<RKTabViewDelegate,HTTPClientDeleagte>
{
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem;
    UILabel *lblDetailText;
    HTTPClient *Client_removebedgecount,*ClientGetBedgecount ,*ClientGetBedgecountNew;
    
    NSString *str_resetbedgecount;
}
@end

@implementation NotificationSubScreen
@synthesize ArrpassDetail;
@synthesize TblAlertdetail,lblTitle;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"Badgecount"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *badgecount=[[NSUserDefaults standardUserDefaults] objectForKey:@"Badgecount"];
    
    if ([badgecount isEqualToString:@"0"]|| badgecount==nil || [badgecount isEqualToString:@""])
    {
        _lbl_badgeCount.hidden=YES;
    }
    else
    {
        _lbl_badgeCount.hidden=NO;
        _lbl_badgeCount.text=str_resetbedgecount;
        [UIApplication sharedApplication].applicationIconBadgeNumber = [str_resetbedgecount intValue];
        
    }
    _lbl_badgeCount.layer.cornerRadius=10;
    _lbl_badgeCount.layer.masksToBounds=YES;
    
    if ([_str_flag isEqualToString:@"noti"])
    {
        [self GetBadgeCount];
    }
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconW"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconW"]];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    
    self.TblAlertdetail.tableFooterView = [[UIView alloc] init];
    self.TblAlertdetail.backgroundColor=[UIColor clearColor];
    
    self.TblAlertdetail.rowHeight =UITableViewAutomaticDimension;
    self.TblAlertdetail.estimatedRowHeight = 95;
    
   // lblTitle.text=[[ArrpassDetail valueForKey:@"message_title"] uppercaseString];
   // lblTitle.text=[[[ArrpassDetail objectAtIndex:0] objectForKey:@"message_title"] uppercaseString];
    
   
    
    NSString *title=[[ArrpassDetail objectAtIndex:0] objectForKey:@"message_title"];
    if(title.length>0)
    {
        lblTitle.text=[[[ArrpassDetail objectAtIndex:0] objectForKey:@"message_title"] uppercaseString];
        
    }
                     else{
    lblTitle.text=[[[[ArrpassDetail objectAtIndex:0] objectForKey:@"aps"]objectForKey:@"mtitle"] uppercaseString];
    }

    
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[FirstTabItem,SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
}
#pragma mark - RKTabViewDelegate
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==0)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==1)
    {
        NSString *strweb=[[NSUserDefaults standardUserDefaults] objectForKey:@"donation"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:strweb]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strweb]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }
    else if (index==2)
    {
        ScheduleSubScreen *vc=[[ScheduleSubScreen alloc]initWithNibName:@"ScheduleSubScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
        ProjectScreen *vc=[[ProjectScreen alloc]initWithNibName:@"ProjectScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
    else if (index==4)
    {
        VideosScreen *vc=[[VideosScreen alloc]initWithNibName:@"VideosScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
    else if (index==5)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}

#pragma mark -
#pragma mark - button Notification
- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark -
#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ArrpassDetail.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"StatusCellIdentifier";
    NotificationSubCell *cell = (NotificationSubCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NotificationSubCell" owner:self options:nil];
        
        cell = [nib objectAtIndex:0];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.layer.borderWidth = 1;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    NSString *date=[[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"alert_date"];
    if(date.length>0)
    {
    
    cell.lblDateTime.text=[[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"alert_date_format"];
    cell.lblEventDetail.text=[[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"message_title"];
    cell.lbl_eventDetailsss.text=[[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"message_details"];
    }
    else
    {
        cell.lblDateTime.text=[[[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"aps"]objectForKey:@"alert_date"];

        cell.lblEventDetail.text=[[[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"aps"]objectForKey:@"mtitle"];

        cell.lbl_eventDetailsss.text=[[[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"aps"]objectForKey:@"mdesc"];
    }
    
//    static NSString *CellIdentifier = @"newFriendCell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil)
//    {
//        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
//    }
//    
//    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//    cell.layer.borderWidth = 1;
//    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    
//    
//    UIImageView *ImgDateIcon=[[UIImageView alloc]initWithFrame:CGRectMake(8, 8, 23, 23)];
//    ImgDateIcon.image=[UIImage imageNamed:@"calendorIcon_blue"];
//    [cell addSubview:ImgDateIcon];
//    
//    
//    
//    UILabel *lblDatetime=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(ImgDateIcon.frame)+8, 8, tableView.frame.size.width-45, 23)];
//    lblDatetime.text=[[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"alert_date_format"];
//    [lblDatetime setFont:[UIFont systemFontOfSize:13]];
//    lblDatetime.textColor=[UIColor colorWithRed:0.47 green:0.63 blue:0.42 alpha:1];
//    [cell addSubview:lblDatetime];
//    
//    
//    
//    UILabel *lblDetailTitle=[[UILabel alloc]initWithFrame:CGRectMake(8, ImgDateIcon.frame.origin.y+ImgDateIcon.frame.size.height+8, tableView.frame.size.width-16, 23)];
//    lblDetailTitle.text=[[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"message_title"];
//    lblDetailTitle.textColor=[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
//    [cell addSubview:lblDetailTitle];
//
//    
//    
//    lblDetailText=[[UILabel alloc]initWithFrame:CGRectMake(8, lblDetailTitle.frame.origin.y+lblDetailTitle.frame.size.height+8, tableView.frame.size.width-16, 120)];
//    lblDetailText.numberOfLines=0;
//    [lblDetailText setFont:[UIFont systemFontOfSize:13]];
//    lblDetailText.text=[[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"message_details"];
//    lblDetailText.textColor=[UIColor grayColor];
//    
//    CGRect addframe = lblDetailText.frame;
//    CGSize constraintis = CGSizeMake(CGRectGetWidth(lblDetailText.frame), MAXFLOAT);
//    CGSize sizeis = [lblDetailText sizeThatFits:constraintis];
//    addframe.size.height = sizeis.height;
//    lblDetailText.frame = addframe;
//    
//    [cell addSubview:lblDetailText];
    
    return cell;
    
}
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    UIFont *cellFont = [UIFont systemFontOfSize:13];
//    CGSize maximumLabelSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width, CGFLOAT_MAX);
//    float hight=[self getLabelHeight:maximumLabelSize string:[[ArrpassDetail objectAtIndex:indexPath.row] valueForKey:@"message_details"] font:cellFont];
//    
//    if (hight>25)
//    {
//        return hight+110;
//    }
//    return 90.0;
//}

//-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    NSMutableString *strauditType = [[ArrpassDetail objectAtIndex:indexPath.row] objectForKey:@"message_details"];
//    
//    CGRect textRect = [strauditType boundingRectWithSize:(CGSize){225, MAXFLOAT}
//                                                 options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
//                                              attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13.0]}
//                                                 context:nil];
//    CGSize messageSize = textRect.size;
//    return messageSize.height  + 10.0f;
//    
//}
-(CGFloat)getLabelHeight:(CGSize)labelSize string: (NSString *)string font: (UIFont *)font
{
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [string boundingRectWithSize:labelSize
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:font}
                                              context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

#pragma mark - DidReceiveMemoryWarning

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)GetBadgeCount
{
    [SVProgressHUD show];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    NSString *divid=[[NSUserDefaults standardUserDefaults]valueForKey:@"DivID"];
    
  
    [parameters setValue:divid forKey:@"reg_id"];
    
    
    NSString *registerURL = [NSString stringWithFormat:@"%@get_badge_count.php",kAPIURL];
    ClientGetBedgecount = [[HTTPClient alloc] init];
    ClientGetBedgecount.delegate = self;
    [ClientGetBedgecount getResponseFromAPI:registerURL andParameters:parameters];
}
-(void)GetBadgeCount1
{
    [SVProgressHUD show];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    NSString *divid=[[NSUserDefaults standardUserDefaults]valueForKey:@"DivID"];
    
    
    [parameters setValue:divid forKey:@"reg_id"];
    
    
    NSString *registerURL = [NSString stringWithFormat:@"%@get_badge_count.php",kAPIURL];
    ClientGetBedgecountNew = [[HTTPClient alloc] init];
    ClientGetBedgecountNew.delegate = self;
    [ClientGetBedgecountNew getResponseFromAPI:registerURL andParameters:parameters];
}

-(void)ResetBadgeCount
{
  //  [SVProgressHUD show];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    NSString *divid=[[NSUserDefaults standardUserDefaults]valueForKey:@"DivID"];
    
    [parameters setValue:str_resetbedgecount forKey:@"badge_count"];
    [parameters setValue:divid forKey:@"reg_id"];
    
    
    NSString *registerURL = [NSString stringWithFormat:@"%@badgecount_reset",kAPIURL];
    Client_removebedgecount = [[HTTPClient alloc] init];
    Client_removebedgecount.delegate = self;
    [Client_removebedgecount getResponseFromAPI:registerURL andParameters:parameters];
}
#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response home screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client == ClientGetBedgecount)
    {
        //NSString *AltListcount=[[response objectForKey:@"alert_list_count"] stringValue];
        
        if ([[[response valueForKey:@"status"] stringValue] isEqualToString:@"200"])
        {
            NSString *str_bedgecount=[NSString stringWithFormat:@"%@",[[response valueForKey:@"data"] valueForKey:@"badge_count"]];
            
            [[NSUserDefaults standardUserDefaults] setObject:str_bedgecount forKey:@"Badgecount"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            NSString *badgecount=[[NSUserDefaults standardUserDefaults] objectForKey:@"Badgecount"];
            if (![badgecount isEqualToString:@"0"])
            {
                int  total_resetcount=[badgecount intValue]-1;
                
                str_resetbedgecount=[NSString stringWithFormat:@"%d",total_resetcount];
                
                [[NSUserDefaults standardUserDefaults] setObject:str_resetbedgecount forKey:@"Badgecount"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [self ResetBadgeCount];

            }
            else
            {
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[badgecount intValue]];
                
            }
          //  [[NSNotificationCenter defaultCenter]postNotificationName:@"begdecount" object:nil];
            
        }
    }
    else if (client==Client_removebedgecount)
    {
        NSLog(@"response");
//        NSString *str_bedgecount=[NSString stringWithFormat:@"%@",[[response valueForKey:@"data"] valueForKey:@"badge_count"]];
//        
//        [[NSUserDefaults standardUserDefaults] setObject:str_bedgecount forKey:@"Badgecount"];
//        [[NSUserDefaults standardUserDefaults]synchronize];
        NSString *badgecount=[[NSUserDefaults standardUserDefaults] objectForKey:@"Badgecount"];
        
        if ([badgecount isEqualToString:@"0"]|| badgecount==nil)
        {
            _lbl_badgeCount.hidden=YES;
        }
        else
        {
            _lbl_badgeCount.hidden=NO;
            _lbl_badgeCount.text=str_resetbedgecount;
        }
        _lbl_badgeCount.layer.cornerRadius=10;
        _lbl_badgeCount.layer.masksToBounds=YES;
         [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[badgecount intValue]];
        [UIApplication sharedApplication].applicationIconBadgeNumber = [str_resetbedgecount intValue];
        [[NSUserDefaults standardUserDefaults] setValue:str_resetbedgecount forKey:@"Badgecount"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
}
- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

@end
