//
//  NotificationSubScreen.h
//  KSIJ Mumbai
//
//  Created by Kishan on 14/12/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RKTabView.h>
#import "HomeScreen.h"
#import "DonationScreen.h"
#import "ScheduleScreen.h"
#import "ProjectScreen.h"
#import "VideosScreen.h"
#import "ContactScreen.h"
#import "NotificationScreen.h"

#import "Constants.h"
#import "AFNetworking.h"
#import "HTTPClient.h"
#import "SVProgressHUD.h"
#import <UIImageView+WebCache.h>

#import "NotificationSubCell.h"

@interface NotificationSubScreen : UIViewController
{
    
}
@property (strong, nonatomic) NSMutableArray *ArrpassDetail;
@property (weak, nonatomic) IBOutlet UILabel *lbl_badgeCount;

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UITableView *TblAlertdetail;
@property (nonatomic,retain)NSString *str_flag;
- (IBAction)btnNoti:(id)sender;

@end
