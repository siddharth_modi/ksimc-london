//
//  NotificationScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 21/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "NotificationScreen.h"

@interface NotificationScreen ()<RKTabViewDelegate,HTTPClientDeleagte>
{
    HTTPClient *ClientNotiList ,*ClientBadgeCount;
    NSMutableArray *ArrNotiList;
    
    
    
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem;
}
@end

@implementation NotificationScreen
@synthesize viewNoti;
@synthesize TblNotiList;


- (void)viewDidLoad
{
    [super viewDidLoad];
//    [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"Badgecount"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconW"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconW"]];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    
    viewNoti.layer.borderWidth = 1;
    viewNoti.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [viewNoti setHidden:YES];
    
//    .viewNoti.borderWidth = 1;
//    _view_noNotification.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    [_view_noNotification setHidden:YES];

    
    self.TblNotiList.tableFooterView = [[UIView alloc] init];
    self.TblNotiList.backgroundColor=[UIColor clearColor];
   
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[FirstTabItem,SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
}
#pragma mark - RKTabViewDelegate
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==0)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==1)
    {
        NSString *strweb=[[NSUserDefaults standardUserDefaults] objectForKey:@"donation"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:strweb]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strweb]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }
    
    else if (index==2)
    {
        ScheduleSubScreen *vc=[[ScheduleSubScreen alloc]initWithNibName:@"ScheduleSubScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
        ProjectScreen *vc=[[ProjectScreen alloc]initWithNibName:@"ProjectScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
    else if (index==4)
    {
        VideosScreen *vc=[[VideosScreen alloc]initWithNibName:@"VideosScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
    else if (index==5)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [self GetNotificationList];
}
//-(void)RemoveBagdecount
//{
//    // [SVProgressHUD show];
//    NSString *registerURL = [NSString stringWithFormat:@"%@get_badge_count.php",kAPIURL];
//    
//    NSString *str_token=[[NSUserDefaults standardUserDefaults] objectForKey:@"DivID"];
//    
//    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
//    [dic setValue:str_token forKey:@"reg_id"];
//    ClientremoveBadgeCount = [[HTTPClient alloc] init];
//    ClientremoveBadgeCount.delegate = self;
//    
//    [ClientremoveBadgeCount getResponseFromAPI:registerURL andParameters:dic];
//}

#pragma mark - GetNotiFicationList

-(void)ResetBadgeCount
{
    [SVProgressHUD show];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
       NSString *divid=[[NSUserDefaults standardUserDefaults]valueForKey:@"DivID"];
    
    [parameters setValue:@"0" forKey:@"badge_count"];
    [parameters setValue:divid forKey:@"reg_id"];

    
    NSString *registerURL = [NSString stringWithFormat:@"%@badgecount_reset",kAPIURL];
    ClientBadgeCount = [[HTTPClient alloc] init];
    ClientBadgeCount.delegate = self;
    [ClientBadgeCount getResponseFromAPI:registerURL andParameters:parameters];
}
#pragma mark - GetNotiFicationList
-(void)GetNotificationList
{
    [SVProgressHUD show];
    
    NSString *registerURL = [NSString stringWithFormat:@"%@get_alert_data_web.php",kAPIURL];
    ClientNotiList = [[HTTPClient alloc] init];
    ClientNotiList.delegate = self;
    [ClientNotiList getResponseFromAPI:registerURL andParameters:nil];
}

#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response home screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client == ClientNotiList)
    {
        NSString *AltListcount=[[response objectForKey:@"alert_list_count"] stringValue];
        
        if (AltListcount!=0)
        {
            ArrNotiList=[[NSMutableArray alloc]init];
            ArrNotiList=[response objectForKey:@"alert_list"];
            
            if (ArrNotiList.count!=0)
            {
                viewNoti.hidden=YES;
            }
            else
            {
                viewNoti.hidden=NO;
            }
            [TblNotiList reloadData];
            
            [self ResetBadgeCount];
        }
        else
        {
            [viewNoti setHidden:NO];
        }
    }
    else if (client==ClientBadgeCount)
    {
        NSLog(@"response");
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"Badgecount"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
}
- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


#pragma mark -
#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ArrNotiList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StatusCellIdentifier";
    NotificationCell *cell = (NotificationCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NotificationCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.layer.borderWidth = 1;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    cell.lblDateTime.text =[[ArrNotiList objectAtIndex:indexPath.row] objectForKey:@"alert_date_format"];
    cell.lblEventDetail.text=[[ArrNotiList objectAtIndex:indexPath.row] objectForKey:@"message_title"];
    cell.lblEventDetail.textColor=[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
    
    [cell.lblEventDetail sizeToFit];
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationSubScreen *vc=[[NotificationSubScreen alloc]initWithNibName:@"NotificationSubScreen" bundle:nil];
    
    NSMutableArray *Temp=[[NSMutableArray alloc]init];
    [Temp addObject:[ArrNotiList objectAtIndex:indexPath.row]];
    vc.ArrpassDetail=Temp;
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark -
#pragma mark - button Notification

- (IBAction)btnNoti:(id)sender
{
    [self GetNotificationList];
}

#pragma mark - DidReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
