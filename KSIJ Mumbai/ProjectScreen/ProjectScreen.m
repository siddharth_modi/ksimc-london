//
//  ProjectScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 21/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "ProjectScreen.h"

@interface ProjectScreen ()<RKTabViewDelegate,HTTPClientDeleagte>
{
     HTTPClient *ClientGetProjectList,*ClientGetBedgecount;
    NSMutableArray *ArrProjectList;
    
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem;
}
@end

@implementation ProjectScreen
@synthesize tblProjectList;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _lbl_badgeCount.layer.cornerRadius=10;
    _lbl_badgeCount.layer.masksToBounds=YES;

    
    NSString *badgecount=[[NSUserDefaults standardUserDefaults] objectForKey:@"Badgecount"];
    
    if ([badgecount isEqualToString:@"0"]|| badgecount==nil)
    {
        _lbl_badgeCount.hidden=YES;
    }
    else
    {
        _lbl_badgeCount.hidden=NO;
        _lbl_badgeCount.text=badgecount;
    }
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconW"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconY"]];
    
    FourTabItem.tabState = TabStateEnabled;
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getBadgecount4) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    [self GetProjectList];
    
    self.tblProjectList.tableFooterView = [[UIView alloc] init];
    self.tblProjectList.backgroundColor=[UIColor clearColor];
}
-(void)getBadgecount4
{
    // [SVProgressHUD show];
    NSString *registerURL = [NSString stringWithFormat:@"%@get_badge_count.php",kAPIURL];
    
    NSString *str_token=[[NSUserDefaults standardUserDefaults] objectForKey:@"DivID"];
    
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    [dic setValue:str_token forKey:@"reg_id"];
    ClientGetBedgecount = [[HTTPClient alloc] init];
    ClientGetBedgecount.delegate = self;
    
    [ClientGetBedgecount getResponseFromAPI:registerURL andParameters:dic];
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[FirstTabItem,SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
}

#pragma mark -
#pragma mark - TabBar Delegate methods
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==0)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==1)
    {
        NSString *strweb=[[NSUserDefaults standardUserDefaults] objectForKey:@"donation"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:strweb]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strweb]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        [self setOnceagain];
        
    }
    
    else if (index==2)
    {
        ScheduleSubScreen *vc=[[ScheduleSubScreen alloc]initWithNibName:@"ScheduleSubScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==4)
    {
        VideosScreen *vc=[[VideosScreen alloc]initWithNibName:@"VideosScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
    else if (index==5)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}

#pragma mark -
#pragma mark - Button notification
- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark -
#pragma mark - GetProjectList
-(void)GetProjectList
{
    [SVProgressHUD show];
    
    NSString *registerURL = [NSString stringWithFormat:@"%@get_news_list.php",kAPIURL];
    ClientGetProjectList = [[HTTPClient alloc] init];
    ClientGetProjectList.delegate = self;
    [ClientGetProjectList getResponseFromAPI:registerURL andParameters:nil];
}
#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
   // NSLog(@"Response Project screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client== ClientGetProjectList)
    {
        if ([[[response valueForKey:@"status"] stringValue] isEqualToString:@"200"])
        {
            ArrProjectList=[[NSMutableArray alloc]init];
            ArrProjectList=[[response objectForKey:@"data"] valueForKey:@"news"];
            [self getBadgecount4];
            [tblProjectList reloadData];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:[response objectForKey:@"data"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else if (client==ClientGetBedgecount)
    {
        if ([[[response valueForKey:@"status"] stringValue] isEqualToString:@"200"])
        {
            NSString *str_bedgecount=[NSString stringWithFormat:@"%@",[[response valueForKey:@"data"] valueForKey:@"badge_count"]];
            
            [[NSUserDefaults standardUserDefaults] setObject:str_bedgecount forKey:@"Badgecount"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            NSString *badgecount=[[NSUserDefaults standardUserDefaults] objectForKey:@"Badgecount"];
            
            if ([badgecount isEqualToString:@"0"]|| badgecount==nil)
            {
                _lbl_badgeCount.hidden=YES;
            }
            else
            {
                _lbl_badgeCount.hidden=NO;
                _lbl_badgeCount.text=badgecount;
            }
            _lbl_badgeCount.layer.cornerRadius=10;
            _lbl_badgeCount.layer.masksToBounds=YES;
            //  [[NSNotificationCenter defaultCenter]postNotificationName:@"begdecount" object:nil];
            
        }
    }
}

- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark -
#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ArrProjectList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StatusCellIdentifier";
    ProjectListCell *cell = (ProjectListCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProjectListCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.layer.borderWidth = 0.5;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    NSString *str_Date=[NSString stringWithFormat:@"%@",[self changeYerFormate2:[[ArrProjectList objectAtIndex:indexPath.row] objectForKey:@"created_date"]]];
    cell.lblDate.text =str_Date;
    cell.lblname.text = [[ArrProjectList objectAtIndex:indexPath.row] objectForKey:@"news_title"];
    
   // NSString *url1=[NSString stringWithFormat:@"%@",[[ArrProjectList objectAtIndex:indexPath.row] objectForKey:@"news_img"]];
//    [cell.img_news setShowActivityIndicatorView:YES];
//    [cell.img_news setIndicatorStyle:UIActivityIndicatorViewStyleGray];
//    [cell.img_news sd_setImageWithURL:[NSURL URLWithString:url1] placeholderImage:[UIImage imageNamed:@"no-image"]];
    //cell.img_news.contentMode=UIViewContentModeScaleAspectFit;

    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProjectDetailscreen *vc=[[ProjectDetailscreen alloc]initWithNibName:@"ProjectDetailscreen" bundle:nil];
    vc.ArrPassData=[ArrProjectList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:NO];
}
#pragma mark - Date changer Method
-(NSString *)changeYerFormate:(NSString *)dateis
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *date = [dateFormatter dateFromString:dateis];
    dateFormatter.dateFormat = @"dd MMMM yyyy";
    return [dateFormatter stringFromDate:date];
}

-(NSString *)changeYerFormate2:(NSString *)dateis
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *date = [dateFormatter dateFromString:dateis];
    dateFormatter.dateFormat = @"dd MMMM yyyy";
    return [dateFormatter stringFromDate:date];
}
#pragma mark -
#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)setOnceagain
{
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconW"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconY"]];
    
    FourTabItem.tabState = TabStateEnabled;
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[FirstTabItem, SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
    
}
@end
