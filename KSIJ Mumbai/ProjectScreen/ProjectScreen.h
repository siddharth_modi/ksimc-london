//
//  ProjectScreen.h
//  KSIJ Mumbai
//
//  Created by Kishan on 21/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RKTabView.h>
#import "HomeScreen.h"
#import "DonationScreen.h"
#import "ScheduleScreen.h"
#import "NotificationScreen.h"
#import "ProjectDetailscreen.h"

#import "Constants.h"
#import "AFNetworking.h"
#import "HTTPClient.h"
#import "SVProgressHUD.h"
#import <UIImageView+WebCache.h>

#import "ProjectListCell.h"

@interface ProjectScreen : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    
}
- (IBAction)btnNoti:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_badgeCount;
@property (strong, nonatomic) IBOutlet UITableView *tblProjectList;

@end
