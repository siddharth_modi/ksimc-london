//
//  Video2ViewController.h
//  KSIJ Mumbai
//
//  Created by mac  on 1/23/17.
//  Copyright © 2017 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Video2ViewController : UIViewController
- (IBAction)btn_back:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *ViewLiveStream;
@property (strong, nonatomic) IBOutlet UIView *ViewMediaLibrary;
@property (nonatomic,retain)NSString *str_channelID;
@property (nonatomic,retain)NSString *str_livestreamid1;
- (IBAction)btnLiveStream:(id)sender;
- (IBAction)btnMediaLibrary:(id)sender;
- (IBAction)btnNoti:(id)sender;
@end
