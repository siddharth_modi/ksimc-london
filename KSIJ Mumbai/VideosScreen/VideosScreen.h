//
//  VideosScreen.h
//  KSIJ Mumbai
//
//  Created by Kishan on 21/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RKTabView.h>
#import "HomeScreen.h"
#import "DonationScreen.h"
#import "ScheduleScreen.h"
#import "ProjectScreen.h"
#import "NotificationScreen.h"
#import "MedialibraryScreen.h"
#import "LiveStreamScreen.h"


#import "Constants.h"
#import "AFNetworking.h"
#import "HTTPClient.h"
#import "SVProgressHUD.h"
#import <UIImageView+WebCache.h>


@interface VideosScreen : UIViewController
{
    
}
@property (strong, nonatomic) IBOutlet UIView *ViewLiveStream;
@property (strong, nonatomic) IBOutlet UIView *ViewMediaLibrary;

- (IBAction)btnLiveStream:(id)sender;
- (IBAction)btnMediaLibrary:(id)sender;
- (IBAction)btnNoti:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_channel1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_channel2;

@property (weak, nonatomic) IBOutlet UILabel *lbl_badgeCount;
@end
