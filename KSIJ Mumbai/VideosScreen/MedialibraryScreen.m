//
//  MedialibraryScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 26/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "MedialibraryScreen.h"

@interface MedialibraryScreen ()<RKTabViewDelegate,HTTPClientDeleagte>
{
    
     RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem;
    
    NSMutableArray *ArrVideoList;
    NSString *pageToken;
    NSString *PreviosToken;
    
    HTTPClient *ClientContactList;
    NSString *VideoID;
    
    
}
@end

@implementation MedialibraryScreen
@synthesize tblChanleList;
@synthesize viewHeader,txtSearch,btnSearch;
@synthesize viewFooter,btnNext,btnPrevious;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconW"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconW"]];
        
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconY"]];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    
    viewHeader.layer.borderWidth = 1;
    viewHeader.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    btnSearch.clipsToBounds = YES;
    btnSearch.layer.cornerRadius =5;
    
//    txtSearch.layer.shadowColor = [UIColor darkGrayColor].CGColor;
//    txtSearch.layer.shadowOffset = CGSizeMake(0, 1);
//    txtSearch.layer.cornerRadius=3;
//    txtSearch.layer.borderWidth=0.5;
//    txtSearch.layer.borderColor=[UIColor lightGrayColor].CGColor;
//    txtSearch.layer.shadowOpacity = 1;
//    txtSearch.layer.shadowRadius = 1.0;
    
    btnNext.clipsToBounds = YES;
    btnNext.layer.cornerRadius =5;
    
    btnPrevious.clipsToBounds = YES;
    btnPrevious.layer.cornerRadius =5;
    
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[FirstTabItem,SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
}
#pragma mark -
#pragma mark - TabBar Delegate methods
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==0)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==1)
    {
        NSString *strweb=[[NSUserDefaults standardUserDefaults] objectForKey:@"donation"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:strweb]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strweb]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }
    else if (index==2)
    {
        ScheduleSubScreen *vc=[[ScheduleSubScreen alloc]initWithNibName:@"ScheduleSubScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
        ProjectScreen *vc=[[ProjectScreen alloc]initWithNibName:@"ProjectScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
    else if (index==4)
    {
        VideosScreen *vc=[[VideosScreen alloc]initWithNibName:@"VideosScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
    else if (index==5)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}

#pragma mark - Button Clicked Event
- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

- (IBAction)btnSearch:(id)sender
{
    NSString *str1=@"http://www.youtube.com/user/";
    NSString *url=[NSString stringWithFormat:@"%@%@",str1,VideoID];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",url]]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",url]]];
    }
}

- (IBAction)btnNext:(id)sender
{
    [self GetChnnelList:pageToken];
}

- (IBAction)btnprevios:(id)sender
{
    [self GetChnnelList:PreviosToken];
}

#pragma mark - ViewWillAppear
-(void)viewWillAppear:(BOOL)animated
{
    [self GetChnnelList:nil];
    
    [self GetContactDeatil];
}
#pragma mark - GetChnnelList
-(void)GetChnnelList:(NSString *)Token
{
    [SVProgressHUD show];

    
    NSString *urlString;
    if ([Token length]>0)
    {
        urlString = [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?key=AIzaSyDvrxZvufNw41vMEj41j-qMbN5S1Rt4w0E&channelId=%@&part=snippet,id&order=date&maxResults=5&pageToken=%@",_str_channelID,Token];
    }
    else
    {
        urlString = [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?key=AIzaSyDvrxZvufNw41vMEj41j-qMbN5S1Rt4w0E&channelId=%@&part=snippet,id&order=date&maxResults=5",_str_channelID];
    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
    {
        
        [SVProgressHUD dismiss];
        
       // NSLog(@"JSON: %@", responseObject);
        pageToken = [responseObject valueForKey:@"nextPageToken"];

        PreviosToken=[responseObject valueForKey:@"prevPageToken"];
        
        
        ArrVideoList=[[NSMutableArray alloc]init];
        ArrVideoList=[responseObject objectForKey:@"items"];
        [tblChanleList reloadData];
        
       // self.tblChanleList.tableHeaderView = viewHeader;
        self.tblChanleList.tableFooterView = viewFooter;
        
        if (PreviosToken==nil)
        {
            [btnPrevious setHidden:YES];
        }
        else
        {
            [btnPrevious setHidden:NO];
        }
        
        if (pageToken==nil)
        {
            [btnNext setHidden:YES];
        }
        else
        {
            [btnNext setHidden:NO];
        }
        
        [tblChanleList setContentOffset:CGPointZero animated:YES];
        
    }
    failure:^(NSURLSessionTask *operation, NSError *error)
    {
        NSLog(@"Error: %@", error);
        
        [SVProgressHUD dismiss];
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }];
    
}

#pragma mark -
#pragma mark - Tableview Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ArrVideoList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *headerView = [[UIView alloc] init];
//    headerView.backgroundColor = [UIColor clearColor];
//    return headerView;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StatusCellIdentifier";
    MediaCell *cell = (MediaCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MediaCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.layer.borderWidth = 0.5;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    NSLog(@"%@",[ArrVideoList objectAtIndex:indexPath.section]);
    
    NSString *str_liveBroadcast=[[[ArrVideoList objectAtIndex:indexPath.section] objectForKey:@"snippet"] objectForKey:@"liveBroadcastContent"];
    NSLog(@"%@",str_liveBroadcast);
    
    cell.lbltitle.text = [[[ArrVideoList objectAtIndex:indexPath.section] objectForKey:@"snippet"] objectForKey:@"title"];
    
    NSString *url1=[[[[[ArrVideoList objectAtIndex:indexPath.section]objectForKey:@"snippet"] objectForKey:@"thumbnails"] objectForKey:@"medium"] objectForKey:@"url"];
    [cell.imgVideo setShowActivityIndicatorView:YES];
    [cell.imgVideo setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cell.imgVideo sd_setImageWithURL:[NSURL URLWithString:url1] placeholderImage:[UIImage imageNamed:@"no-image"]];
    cell.imgVideo.contentMode=UIViewContentModeScaleToFill;
    
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *vId = [[[ArrVideoList objectAtIndex:indexPath.section] valueForKey:@"id"] valueForKey:@"videoId"];

    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.youtube.com/watch?v=%@",vId]]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.youtube.com/watch?v=%@",vId]]];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - DidReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - GetContactDeatil
-(void)GetContactDeatil
{
    [SVProgressHUD show];
    NSString *registerURL = [NSString stringWithFormat:@"%@setting_web.php",kAPIURL];
    ClientContactList = [[HTTPClient alloc] init];
    ClientContactList.delegate = self;
    [ClientContactList getResponseFromAPI:registerURL andParameters:nil];
}
#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response Project screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client== ClientContactList)
    {
        VideoID=[[response objectForKey:@"setting"] objectForKey:@"youtube_id_libary"];
        
    }
}
- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


- (IBAction)btn_back:(id)sender
{
     [self.navigationController popViewControllerAnimated:NO];
}
@end
