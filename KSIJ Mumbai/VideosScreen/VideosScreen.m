//
//  VideosScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 21/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "VideosScreen.h"
#import "Video2ViewController.h"

@interface VideosScreen ()<RKTabViewDelegate,HTTPClientDeleagte>
{
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem;
    
    HTTPClient *Client_SettingApi,*ClientGetBedgecount;
    NSMutableArray *arr_channellist;
    NSString *str_channelId1,*Str_ChannelId2,*str_livestreamid1,*str_livestream_id2;
}
@end

@implementation VideosScreen

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _lbl_badgeCount.layer.cornerRadius=10;
    _lbl_badgeCount.layer.masksToBounds=YES;

    NSString *badgecount=[[NSUserDefaults standardUserDefaults] objectForKey:@"Badgecount"];
    
    if ([badgecount isEqualToString:@"0"]|| badgecount==nil)
    {
        _lbl_badgeCount.hidden=YES;
    }
    else
    {
        _lbl_badgeCount.hidden=NO;
        _lbl_badgeCount.text=badgecount;
    }
    
    _ViewLiveStream.clipsToBounds = YES;
    _ViewLiveStream.layer.cornerRadius =5;
    
    _ViewMediaLibrary.clipsToBounds = YES;
    _ViewMediaLibrary.layer.cornerRadius =5;
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconW"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconW"]];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconY"]];
    
    FiveTabItem.tabState = TabStateEnabled;
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getBadgecount6) name:UIApplicationWillEnterForegroundNotification object:nil];
    [self GetContactDeatil];
}
-(void)getBadgecount6
{
    // [SVProgressHUD show];
    NSString *registerURL = [NSString stringWithFormat:@"%@get_badge_count.php",kAPIURL];
    NSString *str_token=[[NSUserDefaults standardUserDefaults] objectForKey:@"DivID"];
    
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    [dic setValue:str_token forKey:@"reg_id"];
    ClientGetBedgecount = [[HTTPClient alloc] init];
    ClientGetBedgecount.delegate = self;
    
    [ClientGetBedgecount getResponseFromAPI:registerURL andParameters:dic];
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[FirstTabItem,SecondTabItem,  ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
    
    
}


#pragma mark - TabBar Delegate Method
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    if (index==0)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==1)
    {
        NSString *strweb=[[NSUserDefaults standardUserDefaults] objectForKey:@"donation"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:strweb]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strweb]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        [self setOnceagain];
    }
    
    else if (index==2)
    {
        ScheduleSubScreen *vc=[[ScheduleSubScreen alloc]initWithNibName:@"ScheduleSubScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
        ProjectScreen *vc=[[ProjectScreen alloc]initWithNibName:@"ProjectScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
    else if (index==5)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}

#pragma mark - Button Clicked Event
- (IBAction)btnLiveStream:(id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",str_channelId1]]];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
//    Video2ViewController *vc=[[Video2ViewController alloc]initWithNibName:@"Video2ViewController" bundle:nil];
//    vc.str_channelID=str_channelId1;
//    vc.str_livestreamid1=str_livestreamid1;
//    [self.navigationController pushViewController:vc animated:NO];
}

- (IBAction)btnMediaLibrary:(id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",Str_ChannelId2]]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",Str_ChannelId2]]];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }

//    Video2ViewController *vc=[[Video2ViewController alloc]initWithNibName:@"Video2ViewController" bundle:nil];
//    vc.str_channelID=str_channelId1;
//    vc.str_livestreamid1=str_livestream_id2;
//    [self.navigationController pushViewController:vc animated:NO];
}

- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - GetContactDeatil
-(void)GetContactDeatil
{
    [SVProgressHUD show];
    NSString *registerURL = [NSString stringWithFormat:@"%@setting_web.php",kAPIURL];
    Client_SettingApi = [[HTTPClient alloc] init];
    Client_SettingApi.delegate = self;
    [Client_SettingApi getResponseFromAPI:registerURL andParameters:nil];
}
#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response Project screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client== Client_SettingApi)
    {
        arr_channellist=[[NSMutableArray alloc]init];
        
       // arr_channellist=[[response objectForKey:@"contact_us"] objectForKey:@"address_list"];
        
        NSMutableArray *arr=[[NSMutableArray alloc] init];
        
        arr_channellist=[response objectForKey:@"setting"];
        
        
        str_channelId1=[[response objectForKey:@"setting"] valueForKey:@"youtube_id_libary"];
        Str_ChannelId2=[[response objectForKey:@"setting"] valueForKey:@"youtube_id_libary_channel2"];

        str_livestreamid1=[[response objectForKey:@"setting"] valueForKey:@"youtube_id_livestrem"];
        str_livestream_id2=[[response objectForKey:@"setting"] valueForKey:@"youtube_id_livestrem_channel2"];
        
        
        _lbl_channel1.text=[[response objectForKey:@"setting"] valueForKey:@"youtube_channel1_title"];
        _lbl_channel2.text=[[response objectForKey:@"setting"] valueForKey:@"youtube_channel2_title"];
        
        NSLog(@"%@",arr);
        
        [self getBadgecount6];
        
    }
    if (client==ClientGetBedgecount)
    {
        if ([[[response valueForKey:@"status"] stringValue] isEqualToString:@"200"])
        {
            NSString *str_bedgecount=[NSString stringWithFormat:@"%@",[[response valueForKey:@"data"] valueForKey:@"badge_count"]];
            
            [[NSUserDefaults standardUserDefaults] setObject:str_bedgecount forKey:@"Badgecount"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            NSString *badgecount=[[NSUserDefaults standardUserDefaults] objectForKey:@"Badgecount"];
            
            if ([badgecount isEqualToString:@"0"]|| badgecount==nil)
            {
                _lbl_badgeCount.hidden=YES;
            }
            else
            {
                _lbl_badgeCount.hidden=NO;
                _lbl_badgeCount.text=badgecount;
            }
            _lbl_badgeCount.layer.cornerRadius=10;
            _lbl_badgeCount.layer.masksToBounds=YES;
            //  [[NSNotificationCenter defaultCenter]postNotificationName:@"begdecount" object:nil];
            
        }
    }

}

- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
-(void)setOnceagain
{
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconW"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconW"]];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconY"]];
    
    FiveTabItem.tabState = TabStateEnabled;
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[FirstTabItem, SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
    
}
@end
