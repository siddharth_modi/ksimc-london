//
//  ScheduleSubScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 27/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "ScheduleSubScreen.h"

@interface ScheduleSubScreen ()<RKTabViewDelegate>
{
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem;
    HTTPClient *ClientGetBedgecount;
}
@end

@implementation ScheduleSubScreen
@synthesize viewEvent;
@synthesize ArrEvt,Tblevent;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    _lbl_badgeCount.layer.cornerRadius=10;
    _lbl_badgeCount.layer.masksToBounds=YES;

    NSString *badgecount=[[NSUserDefaults standardUserDefaults] objectForKey:@"Badgecount"];
    
   if ([badgecount isEqualToString:@"0"]|| badgecount==nil)
    {
        _lbl_badgeCount.hidden=YES;
    }
    else
    {
        _lbl_badgeCount.hidden=NO;
        _lbl_badgeCount.text=badgecount;
    }

    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconW"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconY"]];
    
    ThirdTabItem.tabState = TabStateEnabled;

    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconW"]];
        
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getBadgecount2) name:UIApplicationWillEnterForegroundNotification object:nil];
    
      //_lblTitle.text=[[ArrEvt objectAtIndex:0] valueForKey:@"EventLocation"];
    NSLog(@"%@",_str_Name);
//    if (ArrEvt.count!=0)
//    {
//        [viewEvent setHidden:YES];
//        
//        NSLog(@"%@",[[ArrEvt objectAtIndex:0]valueForKey:@"EventLocation"]);
//        
//        if ([[[ArrEvt objectAtIndex:0]valueForKey:@"EventLocation"] isEqualToString:@""])
//        {
//            _lblTitle.text=@"PROGRAMS & SCHEDULES";
//        }
//        else
//        {
//            _lblTitle.text=_str_Name;
//
//        }
//    }
//    else
//    {
//        _lblTitle.text=_str_Name;
//    }
    [self GetScheduleList];

    viewEvent.hidden=YES;
    self.Tblevent.tableFooterView = [[UIView alloc] init];
    self.Tblevent.backgroundColor=[UIColor clearColor];
    
   
    
    self.Tblevent.rowHeight =UITableViewAutomaticDimension;
    self.Tblevent.estimatedRowHeight = 75;
     _lblTitle.text=@"PROGRAMS & SCHEDULES";
    
}

-(void)getBadgecount2
{
    // [SVProgressHUD show];
    NSString *registerURL = [NSString stringWithFormat:@"%@get_badge_count.php",kAPIURL];
    
    NSString *str_token=[[NSUserDefaults standardUserDefaults] objectForKey:@"DivID"];
    
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    [dic setValue:str_token forKey:@"reg_id"];
    ClientGetBedgecount = [[HTTPClient alloc] init];
    ClientGetBedgecount.delegate = self;
    
    [ClientGetBedgecount getResponseFromAPI:registerURL andParameters:dic];
}

-(void)GetScheduleList
{
    [SVProgressHUD show];
    NSString *registerURL = [NSString stringWithFormat:@"%@get_events_data_web.php",kAPIURL];
    ClientGetSchedulesList = [[HTTPClient alloc] init];
    ClientGetSchedulesList.delegate = self;
    [ClientGetSchedulesList getResponseFromAPI:registerURL andParameters:nil];
}
#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    // NSLog(@"Response Schedule screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client== ClientGetSchedulesList)
    {
        ArrEvt=[response objectForKey:@"event"];
        
        NSString *str_Event=[NSString stringWithFormat:@"%@",ArrEvt];
        
        
        if ([str_Event isEqualToString:@""])
        {
            viewEvent.hidden=NO;
        }
        else
        {
            [Tblevent reloadData];
            viewEvent.hidden=YES;
        }
       [self getBadgecount2];
    }
    else if (client==ClientGetBedgecount)
    {
        if ([[[response valueForKey:@"status"] stringValue] isEqualToString:@"200"])
        {
            NSString *str_bedgecount=[NSString stringWithFormat:@"%@",[[response valueForKey:@"data"] valueForKey:@"badge_count"]];
            
            [[NSUserDefaults standardUserDefaults] setObject:str_bedgecount forKey:@"Badgecount"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            NSString *badgecount=[[NSUserDefaults standardUserDefaults] objectForKey:@"Badgecount"];
            
            if ([badgecount isEqualToString:@"0"]|| badgecount==nil)
            {
                _lbl_badgeCount.hidden=YES;
            }
            else
            {
                _lbl_badgeCount.hidden=NO;
                _lbl_badgeCount.text=badgecount;
            }
            _lbl_badgeCount.layer.cornerRadius=10;
            _lbl_badgeCount.layer.masksToBounds=YES;
            //  [[NSNotificationCenter defaultCenter]postNotificationName:@"begdecount" object:nil];
            
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[FirstTabItem,SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,SixTabItem];
    
    [self.view addSubview:tabView];
    
    viewEvent.layer.borderWidth = 1;
    viewEvent.layer.borderColor = [UIColor lightGrayColor].CGColor;
}
#pragma mark -
#pragma mark - TabBar Delegate methods
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==0)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==1)
    {
        NSString *strweb=[[NSUserDefaults standardUserDefaults] objectForKey:@"donation"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:strweb]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strweb]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        [self setOnceagain];
//        ScheduleSubScreen *vc=[[ScheduleSubScreen alloc]initWithNibName:@"ScheduleSubScreen" bundle:nil];
//        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
        ProjectScreen *vc=[[ProjectScreen alloc]initWithNibName:@"ProjectScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==4)
    {
        VideosScreen *vc=[[VideosScreen alloc]initWithNibName:@"VideosScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==5)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}

#pragma mark - Button Notification
- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark -
#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ArrEvt.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StatusCellIdentifier";
    ScheduleSubCell *cell = (ScheduleSubCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ScheduleSubCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
  
    cell.layer.borderWidth = 0.5;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    NSString *str1=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"EventDateChange"];
    NSString *str2=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"EventStartChange"];
    cell.lblDateTime.text =[NSString stringWithFormat:@"%@ %@",str1,str2];
    
    NSString *str_EventTitle=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"EventTitle"];
    NSString *str2_EventDetail=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"EventDetails"];
    
    cell.lblEventDetail.text=str_EventTitle;
//    if ([str2_EventDetail  isEqualToString:@""])
//    {
//        
//        
//    }
//    else
//    {
//        cell.lblEventDetail.text=[NSString stringWithFormat:@"%@ , %@",str_EventTitle,str2_EventDetail];
//
//    }
    
    NSString *strColor=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"ColorCode"];
    if ([strColor isEqualToString:@"G"])
    {
        cell.lblEventDetail.textColor=[UIColor colorWithRed:0.19 green:0.40 blue:0.04 alpha:1];
    }
    else if ([strColor isEqualToString:@"R"])
    {
        cell.lblEventDetail.textColor=[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
    }
    else
    {
        cell.lblEventDetail.textColor=[UIColor blackColor];
    }
    [cell.lblEventDetail sizeToFit];
    
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CalenderScreen *vc=[[CalenderScreen alloc]initWithNibName:@"CalenderScreen" bundle:nil];
    //vc.ArrEvent=[@[[ArrEvt objectAtIndex:indexPath.row]] mutableCopy];
    vc.PasSelectedDate=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"EventDate"];
    vc.ArrEvent=ArrEvt;
    vc.Str_EventTitle=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"EventTitle"];
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark - didReceiveMemoryWarning

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setOnceagain
{
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconW"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconY"]];
    
    ThirdTabItem.tabState = TabStateEnabled;
    
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconW"]];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[FirstTabItem, SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
    
}
@end
