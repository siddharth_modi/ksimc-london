//
//  ScheduleScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 21/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "ScheduleScreen.h"

@interface ScheduleScreen ()<RKTabViewDelegate,HTTPClientDeleagte>
{
    HTTPClient *ClientGetSchedulesList;
    NSMutableArray *ArrScheduleList;
    NSMutableArray *Arrevent;
    
    NSMutableArray *arr_another;
    NSMutableArray *arr_KSi;
    
    NSMutableArray *arr_finalLocation;
    
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem;
}
@property (nonatomic, strong) HTTPClient *httpClient;

@end

@implementation ScheduleScreen
@synthesize TblSchedules,ArrEvt;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    arr_another=[[NSMutableArray alloc] init];
    
    arr_KSi=[[NSMutableArray alloc] init];
    
    arr_finalLocation=[[NSMutableArray alloc] init];

    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconY"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    
   // ThirdTabItem.tabState = TabStateEnabled;
    
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconW"]];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    
    [self GetScheduleList];
    
    self.TblSchedules.tableFooterView = [[UIView alloc] init];
    self.TblSchedules.backgroundColor=[UIColor clearColor];
}
//-(void)viewDidAppear:(BOOL)animated
//{
//    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
//    tabView.delegate = self;
//    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
//    
//    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
//    
//    tabView.tabItems =@[FirstTabItem, SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
//    
//    [self.view addSubview:tabView];
//    
//    
//}
//-(void)viewDidAppear:(BOOL)animated
//{
//    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
//    tabView.delegate = self;
//    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
//    
//    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
//    
//    tabView.tabItems =@[FirstTabItem, SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,SixTabItem];
//    
//    [self.view addSubview:tabView];
//    
//    viewEvent.layer.borderWidth = 1;
//    viewEvent.layer.borderColor = [UIColor lightGrayColor].CGColor;
//}

#pragma mark -
#pragma mark - TabBar Delegate method
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);

    if (index==0)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==1)
    {
        DonationScreen *vc=[[DonationScreen alloc]initWithNibName:@"DonationScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
        ProjectScreen *vc=[[ProjectScreen alloc]initWithNibName:@"ProjectScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==4)
    {
        VideosScreen *vc=[[VideosScreen alloc]initWithNibName:@"VideosScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==5)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}

#pragma mark -
#pragma mark - button Notification
- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark -
#pragma mark - GetScheduleList
-(void)GetScheduleList
{
    [SVProgressHUD show];
    NSString *registerURL = [NSString stringWithFormat:@"%@get_events_data_web.php",kAPIURL];
    ClientGetSchedulesList = [[HTTPClient alloc] init];
    ClientGetSchedulesList.delegate = self;
    [ClientGetSchedulesList getResponseFromAPI:registerURL andParameters:nil];
}
#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
   // NSLog(@"Response Schedule screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client== ClientGetSchedulesList)
    {
        ArrScheduleList=[[NSMutableArray alloc]init];
        ArrScheduleList=[response objectForKey:@"location_list"];
        
        NSString *strEvCount=[NSString stringWithFormat:@"%@",[response objectForKey:@"event_count"]];
        
        if ([strEvCount intValue]==0)
        {
            arr_finalLocation=ArrScheduleList.mutableCopy;
//            arr_another=[response objectForKey:@"event"];
            ArrEvt=[response objectForKey:@"event"];

            [TblSchedules reloadData];

        }
        else
        {
            
            if (![strEvCount isEqualToString:@"0"])
            {
//                Arrevent=[[NSMutableArray alloc]init];
//                Arrevent=[response objectForKey:@"event"];
                 ArrEvt=[response objectForKey:@"event"];
            }
            
            for (int i=0; i<Arrevent.count; i++)
            {
                if ([[[Arrevent objectAtIndex:i] valueForKey:@"EventLocationID"] isEqualToString:@"0"])
                {
                    [arr_another addObject:[Arrevent objectAtIndex:i]];
                }
                else
                {
                    [arr_KSi addObject:[Arrevent objectAtIndex:i]];
                }
            }
            
            NSLog(@" Array another =%@",arr_another);
            NSLog(@"Array KSI =%@",arr_KSi);
            
            if (arr_another.count!=0)
            {
                NSDictionary *dic=@{@"location":@"Others"};
                NSMutableArray *arraydic=[[NSMutableArray alloc] init];
                
                for (NSMutableDictionary *dic1 in ArrScheduleList)
                {
                    [arr_finalLocation addObject:dic1];
                }
                [arr_finalLocation addObject:dic];
                
                NSLog(@" All data is %@",arraydic);
                
            }
            else
            {
                for (NSMutableDictionary *dic1 in ArrScheduleList)
                {
                    [arr_finalLocation addObject:dic1];
                }
            }
            
            [TblSchedules reloadData];
        }
        
        
       
    }
}

- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark -
#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   // return arr_finalLocation.count;
    return ArrEvt.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *CellIdentifier = @"StatusCellIdentifier";
//    ScheduleCell *cell = (ScheduleCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if(cell == nil)
//    {
//        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ScheduleCell" owner:self options:nil];
//        cell = [nib objectAtIndex:0];
//    }
//    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//    cell.layer.borderWidth = 0.5;
//    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    
//    NSLog(@" Data %@",[[arr_finalLocation objectAtIndex:indexPath.row] objectForKey:@"location"]);
//    cell.lblLocation.text = [[arr_finalLocation objectAtIndex:indexPath.row] objectForKey:@"location"];
//    return cell;
    
    static NSString *CellIdentifier = @"StatusCellIdentifier";
    ScheduleSubCell *cell = (ScheduleSubCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ScheduleSubCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.layer.borderWidth = 1;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    NSString *str1=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"EventDateChange"];
    NSString *str2=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"EventStartChange"];
    cell.lblDateTime.text =[NSString stringWithFormat:@"%@ %@",str1,str2];
    
    NSString *str_EventTitle=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"EventTitle"];
    NSString *str2_EventDetail=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"EventDetails"];
    
    if ([str2_EventDetail  isEqualToString:@""])
    {
        cell.lblEventDetail.text=str_EventTitle;
        
    }
    else
    {
        cell.lblEventDetail.text=[NSString stringWithFormat:@"%@ , %@",str_EventTitle,str2_EventDetail];
        
    }
    
    NSString *strColor=[[ArrEvt objectAtIndex:indexPath.row] objectForKey:@"ColorCode"];
    if ([strColor isEqualToString:@"G"])
    {
        cell.lblEventDetail.textColor=[UIColor colorWithRed:0.19 green:0.40 blue:0.04 alpha:1];
    }
    else if ([strColor isEqualToString:@"R"])
    {
        cell.lblEventDetail.textColor=[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
    }
    else
    {
        cell.lblEventDetail.textColor=[UIColor blackColor];
    }
    [cell.lblEventDetail sizeToFit];
    
    return cell;

    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([[[arr_finalLocation objectAtIndex:indexPath.row] objectForKey:@"location"] isEqualToString:@"Others"])
    {
         ScheduleSubScreen *vc=[[ScheduleSubScreen alloc]initWithNibName:@"ScheduleSubScreen" bundle:nil];
        NSLog(@"arraydata  %@",arr_another);
        NSLog(@"%@",[[arr_finalLocation objectAtIndex:indexPath.row] objectForKey:@"location"]);
        vc.ArrEvt=arr_another;
        vc.str_Name=[[arr_finalLocation objectAtIndex:indexPath.row] objectForKey:@"location"];
        [self.navigationController pushViewController:vc animated:NO];

    }
    else
    {
        ScheduleSubScreen *vc=[[ScheduleSubScreen alloc]initWithNibName:@"ScheduleSubScreen" bundle:nil];
        
        NSString *StrId=[[ArrScheduleList objectAtIndex:indexPath.row] objectForKey:@"id"];
        
        NSMutableArray *temp=[[NSMutableArray alloc]init];
        
        for (int i=0; i<Arrevent.count; i++)
        {
            NSString *locid=[[Arrevent objectAtIndex:i] objectForKey:@"EventLocationID"];
            
            if ([locid isEqualToString:StrId])
            {
                [temp addObject:[Arrevent objectAtIndex:i]];
            }
        }
        vc.ArrEvt=temp;
         vc.str_Name=[[arr_finalLocation objectAtIndex:indexPath.row] objectForKey:@"location"];
        [self.navigationController pushViewController:vc animated:NO];
    }
}
#pragma mark -
#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
