//
//  HTTPClient.h
//  ViUW
//
//  Created by Meet_MacMini on 12/4/15.
//  Copyright © 2015 IndianTTS. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@protocol HTTPClientDeleagte;

@interface HTTPClient : AFHTTPSessionManager

@property (weak, nonatomic) id<HTTPClientDeleagte> delegate;

+ (HTTPClient *)sharedHTTPClient;
- (void)getResponseFromAPI:(NSString *)api andParameters:(NSDictionary *)parameters;


@end

@protocol HTTPClientDeleagte <NSObject>

@optional


// For
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response;
- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error;


@end
