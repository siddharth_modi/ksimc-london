//
//  ContactScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 21/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "ContactScreen.h"
#define MAX_LENGTH 15
@interface ContactScreen ()<RKTabViewDelegate,HTTPClientDeleagte>
{
    HTTPClient *ClientContactList,*ClientNewslater,*ClientGetBedgecount;
    NSMutableArray *ArrAddressList;
    NSMutableArray *ArrContactList;
    
    
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem;
    
}
@end

@implementation ContactScreen

@synthesize viewSub;
@synthesize tblContactList,lblContact;
@synthesize ViewFutter;
@synthesize viewSocial,viewSubscribe,btnSend;
@synthesize txtMail,txtMobileNo;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   _lbl_badgeCount.hidden=YES;

    NSString *badgecount=[[NSUserDefaults standardUserDefaults] objectForKey:@"Badgecount"];
    
    if ([badgecount isEqualToString:@"0"]|| badgecount==nil)
    {
        _lbl_badgeCount.hidden=YES;
        _lbl_badgeCount.layer.cornerRadius=10;
        _lbl_badgeCount.layer.masksToBounds=YES;
    }
    else
    {
        _lbl_badgeCount.hidden=NO;
        _lbl_badgeCount.text=badgecount;
    }
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconW"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconW"]];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconY"]];
    
    SixTabItem.tabState = TabStateEnabled;
    
    viewSocial.layer.borderWidth = 1;
    viewSocial.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    viewSubscribe.layer.borderWidth = 1;
    viewSubscribe.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    btnSend.clipsToBounds = YES;
    btnSend.layer.cornerRadius =5;
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getBadgecount7) name:UIApplicationWillEnterForegroundNotification object:nil];
    self.tblContactList.rowHeight =UITableViewAutomaticDimension;
    self.tblContactList.estimatedRowHeight = 272;
//    txtMail.layer.shadowColor = [UIColor darkGrayColor].CGColor;
//    txtMail.layer.shadowOffset = CGSizeMake(0, 1);
//    txtMail.layer.cornerRadius=3;
//    txtMail.layer.borderWidth=0.5;
//    txtMail.layer.borderColor=[UIColor lightGrayColor].CGColor;
//    txtMail.layer.shadowOpacity = 1;
//    txtMail.layer.shadowRadius = 1.0;
//    
//    txtMobileNo.layer.shadowColor = [UIColor darkGrayColor].CGColor;
//    txtMobileNo.layer.shadowOffset = CGSizeMake(0, 1);
//    txtMobileNo.layer.cornerRadius=3;
//    txtMobileNo.layer.borderWidth=0.5;
//    txtMobileNo.layer.borderColor=[UIColor lightGrayColor].CGColor;
//    txtMobileNo.layer.shadowOpacity = 1;
//    txtMobileNo.layer.shadowRadius = 1.0;
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:)name:UIKeyboardWillShowNotification object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)name:UIKeyboardWillHideNotification object:nil];
    
}
-(void)getBadgecount7
{
    // [SVProgressHUD show];
    NSString *registerURL = [NSString stringWithFormat:@"%@get_badge_count.php",kAPIURL];
    NSString *str_token=[[NSUserDefaults standardUserDefaults] objectForKey:@"DivID"];
    
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    [dic setValue:str_token forKey:@"reg_id"];
    ClientGetBedgecount = [[HTTPClient alloc] init];
    ClientGetBedgecount.delegate = self;
    
    [ClientGetBedgecount getResponseFromAPI:registerURL andParameters:dic];
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[FirstTabItem,SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
    
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self GetContactDeatil];
}
#pragma mark -
#pragma mark - TabBar Delegate methods
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==0)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==1)
    {
        NSString *strweb=[[NSUserDefaults standardUserDefaults] objectForKey:@"donation"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:strweb]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strweb]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        [self setOnceagain];
        
    }
    else if (index==2)
    {
        ScheduleSubScreen *vc=[[ScheduleSubScreen alloc]initWithNibName:@"ScheduleSubScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
        ProjectScreen *vc=[[ProjectScreen alloc]initWithNibName:@"ProjectScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
    else if (index==4)
    {
        VideosScreen *vc=[[VideosScreen alloc]initWithNibName:@"VideosScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}

#pragma mark - Button notification
- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

- (IBAction)btnSend:(id)sender
{
    if ([txtMail.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Please enter email Address." delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (![self validateEmail:[txtMail text]])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Please Enter Valid Email Address !" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
        txtMail.text = @"";
        [txtMail becomeFirstResponder];
    }
    else if ([txtMobileNo.text isEqualToString:@""])
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Please enter Mobile No." delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (txtMobileNo.text.length<8)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Mobile No should be 8 digits." delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else
    {
         [SVProgressHUD show];
        NSDictionary *parameters = @{
                                     @"email" :txtMail.text,
                                     @"tel":txtMobileNo.text
                                     };
        
        
        NSString *registerURL = [NSString stringWithFormat:@"%@newsletters_web.php",kAPIURL];
        ClientNewslater = [[HTTPClient alloc] init];
        ClientNewslater.delegate = self;
        [ClientNewslater getResponseFromAPI:registerURL andParameters:parameters];
        
    }
}

- (IBAction)btnFb:(id)sender
{
    [self.view endEditing:YES];
    
    NSString *strFblink=[ArrContactList valueForKey:@"facebook_link"];
    
    
    NSString *link=[NSString stringWithFormat:@"%@",strFblink];
    
    [[UIApplication sharedApplication] openURL:[NSURL
                                                URLWithString:link]];
}
- (IBAction)btnTwetter:(id)sender
{
    [self.view endEditing:YES];
    
    NSString *strTwetterlink=[ArrContactList valueForKey:@"twitter_link"];
    
    NSString *link=[NSString stringWithFormat:@"%@",strTwetterlink];
    
    [[UIApplication sharedApplication] openURL:[NSURL
                                                URLWithString:link]];
    
}
- (IBAction)btnyoutube:(id)sender
{
    [self.view endEditing:YES];
    
    NSString *stryoutubelink=[ArrContactList valueForKey:@"youtube_link"];
    
    NSString *link=[NSString stringWithFormat:@"https://www.youtube.com/channel/%@",stryoutubelink];
    
    [[UIApplication sharedApplication] openURL:[NSURL
                                                URLWithString:link]];
}

#pragma mark - GetContactDeatil
-(void)GetContactDeatil
{
    [SVProgressHUD show];
    NSString *registerURL = [NSString stringWithFormat:@"%@setting_web.php",kAPIURL];
    ClientContactList = [[HTTPClient alloc] init];
    ClientContactList.delegate = self;
    [ClientContactList getResponseFromAPI:registerURL andParameters:nil];
}
#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response Project screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client== ClientContactList)
    {
        ArrAddressList=[[NSMutableArray alloc]init];
        ArrContactList=[[NSMutableArray alloc]init];
        
        ArrAddressList=[[response objectForKey:@"contact_us"] objectForKey:@"address_list"];
        ArrContactList=[response objectForKey:@"contact_us"];
        
        [self getBadgecount7];
        
        [self SetFramTableview];
        [tblContactList reloadData];
        
        
        self.tblContactList.tableFooterView = ViewFutter;
    }
    else if (client== ClientNewslater)
    {
        NSString *Code=[NSString stringWithFormat:@"%@",[response objectForKey:@"success"]];
        
        if ([Code isEqualToString:@"1"])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:[response objectForKey:@"msg"] delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
            
            txtMail.text=@"";
            txtMobileNo.text=@"";
        }
        else if ([Code isEqualToString:@"0"])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:[response objectForKey:@"msg"] delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    if (client==ClientGetBedgecount)
    {
        if ([[[response valueForKey:@"status"] stringValue] isEqualToString:@"200"])
        {
            NSString *str_bedgecount=[NSString stringWithFormat:@"%@",[[response valueForKey:@"data"] valueForKey:@"badge_count"]];
            
            [[NSUserDefaults standardUserDefaults] setObject:str_bedgecount forKey:@"Badgecount"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            NSString *badgecount=[[NSUserDefaults standardUserDefaults] objectForKey:@"Badgecount"];
            
            if ([badgecount isEqualToString:@"0"]|| badgecount==nil)
            {
                _lbl_badgeCount.hidden=YES;
            }
            else
            {
                _lbl_badgeCount.hidden=NO;
                _lbl_badgeCount.text=badgecount;
            }
            _lbl_badgeCount.layer.cornerRadius=10;
            _lbl_badgeCount.layer.masksToBounds=YES;
            //  [[NSNotificationCenter defaultCenter]postNotificationName:@"begdecount" object:nil];
            
        }
    }

}

- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - SetFramTableview
-(void)SetFramTableview
{
    CGRect currentFrame = tblContactList.frame;
    currentFrame.origin.x=0;
    currentFrame.origin.y=0;
    currentFrame.size.width=viewSub.frame.size.width;
    currentFrame.size.height =viewSub.frame.size.height-10;
    tblContactList.frame = currentFrame;

}

#pragma mark - Tableview Delegate Method
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ArrAddressList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactListCell *cell=(ContactListCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ContactListCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.layer.borderWidth = 1;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    if ([[[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_3"] isEqualToString:@""])
    {
        
        if ([[[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_2"] isEqualToString:@""])
        {
            [cell.p2 setHidden:YES];
            [cell.p3 setHidden:YES];

            cell.p3_TopConstraints.constant=-84;
            cell.img_phoneTopconstraints.constant=10;
        }
        else
        {
            [cell.p3 setHidden:YES];
            NSLog(@"%f",cell.lbl_sleeplinetopconstrains.constant);
            cell.lbl_sleeplinetopconstrains.constant=-42;
            cell.img_phoneTopconstraints.constant=35;
        }
    }
    if ([[[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_2"] isEqualToString:@""])
    {
        [cell.p2 setHidden:YES];
        
        cell.p3_TopConstraints.constant=-42;
        cell.img_phoneTopconstraints.constant=35;

     //   cell.p3_TopConstraints.constant=-42;
     //   cell.img_phoneTopconstraints.constant=25;

//        CGRect FrmImgPhone=cell.imgPhone.frame;
//        FrmImgPhone.origin.y=(cell.p1.frame.origin.y+cell.p1.frame.size.height/2)-(cell.imgPhone.frame.size.height/2);
//        cell.imgPhone.frame=FrmImgPhone;
//        
//        
//        
//        CGRect Frmbtnmail=cell.btnMail.frame;
//        Frmbtnmail.origin.y=cell.p1.frame.origin.y+cell.p1.frame.size.height+5;
//        cell.btnMail.frame=Frmbtnmail;
//        
//        CGRect FrmImgmail=cell.imgMail.frame;
//        FrmImgmail.origin.y=cell.btnMail.frame.origin.y+5;
//        cell.imgMail.frame=FrmImgmail;
//        
//        
//        
//        CGRect FrmbtnWeb=cell.btnWeb.frame;
//        FrmbtnWeb.origin.y=cell.btnMail.frame.origin.y+cell.btnMail.frame.size.height+5;
//        cell.btnWeb.frame=FrmbtnWeb;
//        
//        CGRect FrmImgWeb=cell.imgWeb.frame;
//        FrmImgWeb.origin.y=cell.btnWeb.frame.origin.y+5;
//        cell.imgWeb.frame=FrmImgWeb;
        
       
    }
    
    cell.lbladdress.text=[NSString stringWithFormat:@"%@",[[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"address"]];
    
//    CGSize labelSize = [cell.lbladdress.text sizeWithFont:cell.lbladdress.font
//                                constrainedToSize:cell.lbladdress.frame.size
//                                    lineBreakMode:NSLineBreakByWordWrapping];
//    
//    CGRect frm=cell.lbladdress.frame;
//    frm.size.height=labelSize.height;
//    cell.lbladdress.frame=frm;
    
 
//    UIButton *btnaddress=[[UIButton alloc]init];
//    btnaddress.frame=cell.lbladdress.frame;
//    [cell addSubview:btnaddress];
//    btnaddress.tag=indexPath.section;
    [cell.btn_address addTarget:self action:@selector(BtnAddressClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [cell.btnphone1 setTitle: [[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_1"] forState: UIControlStateNormal];
    cell.btnphone1.tag=indexPath.section;
   // [cell.btnphone1 sizeToFit];
    [cell.btnphone1 addTarget:self action:@selector(BtnPhone1Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.lblPhone1Title.text=[[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_1_heading"];
    cell.lblPhone2title.text=[[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_2_heading"];
    cell.lblPhone3Title.text=[[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_3_heading"];
    [cell.btnPhone2 setTitle: [[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_2"] forState: UIControlStateNormal];
    cell.btnPhone2.tag=indexPath.section;
   // [cell.btnPhone2 sizeToFit];
    [cell.btnPhone2 addTarget:self action:@selector(BtnPhone2Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btnFax setTitle: [[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_3"] forState: UIControlStateNormal];
    cell.btnFax.tag=indexPath.section;
    //[cell.btnFax sizeToFit];
    [cell.btnFax addTarget:self action:@selector(BtnFaxClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [cell.btnMail setTitle: [[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"email"] forState: UIControlStateNormal];
    cell.btnMail.tag=indexPath.section;
   // [cell.btnMail sizeToFit];
    [cell.btnMail addTarget:self action:@selector(BtnMailClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [cell.btnWeb setTitle: [[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"website"] forState: UIControlStateNormal];
    cell.btnWeb.tag=indexPath.section;
   // [cell.btnWeb sizeToFit];
    [cell.btnWeb addTarget:self action:@selector(BtnWebClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//   
//    
//    if ([[[ArrAddressList objectAtIndex:indexPath.section] objectForKey:@"phone_2"] isEqualToString:@""])
//    {
//        return 226;
//    }
//    else
//    {
//       return 300;
//   }
//}

#pragma mark - Button Clicked Event
-(void)BtnAddressClicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    NSLog(@"BtnAddress Clicked %ld",(long)sender.tag);
    
    NSString *add=[[ArrAddressList objectAtIndex:sender.tag] objectForKey:@"address"];
    
    NSString *mapURLStr = [NSString stringWithFormat: @"http://maps.apple.com/?q=%@",add];
    mapURLStr = [mapURLStr stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSURL *url = [NSURL URLWithString:[mapURLStr stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)BtnPhone1Clicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    NSLog(@"BtnPhone1 Clicked %ld",(long)sender.tag);
    
    NSString *phNo =[[ArrAddressList objectAtIndex:sender.tag]objectForKey:@"phone_1"];
    
    phNo = [phNo stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
    {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }

}
-(void)BtnPhone2Clicked:(UIButton *)sender
{
    [self.view endEditing:YES];
//    NSLog(@"BtnPhone2 Clicked %ld",(long)sender.tag);
//    
//    NSString *Mno=[NSString stringWithFormat:@"telprompt://%@",[[ArrAddressList objectAtIndex:sender.tag]objectForKey:@"phone_2"]];
//    
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:Mno]];
    
    
    NSString *strPhone = [[NSString alloc] initWithFormat:@"telprompt:%@",[[ArrAddressList objectAtIndex:sender.tag]objectForKey:@"phone_2"]];
    strPhone = [strPhone stringByReplacingOccurrencesOfString:@" " withString:@""];

    NSURL *phoneURL = [[NSURL alloc] initWithString:strPhone];
    if ([[UIApplication sharedApplication] canOpenURL:phoneURL])
    {
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
//    [[UIApplication sharedApplication] openURL:phoneURL];
//    
//    NSString *phNo =[[ArrAddressList objectAtIndex:sender.tag]objectForKey:@"phone_1"];
//    
//    phNo = [phNo stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
//    

}

-(void)BtnFaxClicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    NSString *strPhone = [[NSString alloc] initWithFormat:@"telprompt:%@",[[ArrAddressList objectAtIndex:sender.tag]objectForKey:@"phone_3"]];
    strPhone = [strPhone stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSURL *phoneURL = [[NSURL alloc] initWithString:strPhone];
    if ([[UIApplication sharedApplication] canOpenURL:phoneURL])
    {
        [[UIApplication sharedApplication] openURL:phoneURL];
    }

    NSLog(@"BtnFax Clicked %ld",(long)sender.tag);
}

-(void)BtnMailClicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    NSLog(@"BtnMail Clicked %ld",(long)sender.tag);
    
    NSString *EmailId=[[ArrAddressList objectAtIndex:sender.tag] objectForKey:@"email"];
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        NSArray *toRecipients = [NSArray arrayWithObjects:EmailId, nil];
        [mailer setToRecipients:toRecipients];
        
        [self presentViewController:mailer animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Your device doesn't support." delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    UIAlertView *alert;
    switch (result)
    {
        case MFMailComposeResultCancelled:
            
            alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Mail canceled" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        case MFMailComposeResultSaved:
            
            alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Mail saved" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        case MFMailComposeResultSent:
            
            alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Mail send" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        case MFMailComposeResultFailed:
            
            alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Mail failed" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        default:
            
            alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Mail not sent" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
    }
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)BtnWebClicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    NSLog(@"BtnWeb Clicked %ld",(long)sender.tag);
    
    NSString *strweb=[[ArrAddressList objectAtIndex:sender.tag] objectForKey:@"website"];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:strweb]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strweb]];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        
        [alert show];
    }
}


#pragma mark - TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    self.activeTextField = textField;
    
    return YES;
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    self.activeTextField = nil;
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==txtMobileNo)
    {
        if (txtMobileNo.text.length >= MAX_LENGTH && range.length == 0)
        {
            return NO; // return NO to not change text
        }
        else
        {
            return YES;
        }
    }
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
#pragma mark-
#pragma mark Keyboard Mange Method
- (void)keyboardWasShown:(NSNotification *)notification
{
    
   [tblContactList setContentSize:CGSizeMake(self.view.frame.size.width, self.tblContactList.frame.size.height)];
    NSDictionary* info = [notification userInfo];
    
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    
    kbRect = [self.view convertRect:kbRect fromView:nil];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbRect.size.height+10, 0.0);
    
    self.tblContactList.contentInset = contentInsets;
    
    self.tblContactList.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbRect.size.height;
    [self.tblContactList scrollRectToVisible:self.activeTextField.frame animated:YES];
}
- (void) keyboardWillHide:(NSNotification *)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    self.tblContactList.contentInset = contentInsets;
    
    self.tblContactList.scrollIndicatorInsets = UIEdgeInsetsZero;
    
    self.tblContactList.contentOffset = CGPointMake(0.0, 0.0);
    
    [self.view endEditing:YES];
}

#pragma mark email validation
- (BOOL)validateEmail:(NSString *)emailStr
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)setOnceagain
{
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconW"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconW"]];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconY"]];
    
    SixTabItem.tabState = TabStateEnabled;
    
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[FirstTabItem, SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
    
}
@end
