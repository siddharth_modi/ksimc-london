//
//  HomeCell.m
//  KSIJ Mumbai
//
//  Created by Kishan on 24/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "HomeCell.h"

@implementation HomeCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}
@synthesize lbldayname,lblyearname1,lblYearName2;
@synthesize lblFajrTime,lblZoharTime,lblImsaakTime,lblSunsetTime,lblMaghribTime,lblSunriseTime,lblMidnightTime;
@synthesize img1,img2,img3,img4,img5,img6,img7;

@synthesize Switch1,Switch2,Switch3,Switch4,Switch5,Switch6,Switch7;


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
