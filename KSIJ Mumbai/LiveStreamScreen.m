//
//  LiveStreamScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 27/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "LiveStreamScreen.h"

@interface LiveStreamScreen ()<RKTabViewDelegate,HTTPClientDeleagte>
{
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem;
    
    HTTPClient *ClientContactList;
    
    NSString *LiveStreamID;
    
}
@end

@implementation LiveStreamScreen
@synthesize imgVideo;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconY"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconW"]];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconY"]];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[FirstTabItem, SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
}
-(void)viewWillAppear:(BOOL)animated
{
    [self GetContactDeatil];
}

#pragma mark -
#pragma mark - TabBar Delegate methods
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==0)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==1)
    {
        NSString *strweb=[[NSUserDefaults standardUserDefaults] objectForKey:@"donation"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:strweb]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strweb]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:@"Invalid url" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        LiveStreamScreen *vc=[[LiveStreamScreen alloc]initWithNibName:@"LiveStreamScreen" bundle:nil];
       
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==2)
    {
        ScheduleScreen *vc=[[ScheduleScreen alloc]initWithNibName:@"ScheduleScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
        ProjectScreen *vc=[[ProjectScreen alloc]initWithNibName:@"ProjectScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==4)
    {
        VideosScreen *vc=[[VideosScreen alloc]initWithNibName:@"VideosScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==5)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}

#pragma mark - Button Notification
- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

- (IBAction)btnPlayVideo:(id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.youtube.com/watch?v=%@",LiveStreamID]]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.youtube.com/watch?v=%@",LiveStreamID]]];
    }
}

- (IBAction)btn_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - GetContactDeatil
-(void)GetContactDeatil
{
    [SVProgressHUD show];
    NSString *registerURL = [NSString stringWithFormat:@"%@setting_web.php",kAPIURL];
    ClientContactList = [[HTTPClient alloc] init];
    ClientContactList.delegate = self;
    [ClientContactList getResponseFromAPI:registerURL andParameters:nil];
}
-(void)GetImagefromUrl
{
    NSString *str1=@"http://img.youtube.com/vi/";
    NSString *str2=@"/0.jpg";
    NSString *url=[NSString stringWithFormat:@"%@%@%@",str1,LiveStreamID,str2];
    
    [imgVideo setShowActivityIndicatorView:YES];
    [imgVideo setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [imgVideo sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"no-image"]];
    imgVideo.contentMode=UIViewContentModeScaleToFill;
}

#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response Project screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client== ClientContactList)
    {
        LiveStreamID=[[response objectForKey:@"setting"] objectForKey:@"youtube_id_livestrem"];
        
        [self GetImagefromUrl];
        
    }
}
- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

@end
