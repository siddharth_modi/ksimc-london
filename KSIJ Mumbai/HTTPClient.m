//
//  HTTPClient.m
//  ViUW
//
//  Created by Meet_MacMini on 12/4/15.
//  Copyright © 2015 IndianTTS. All rights reserved.
//

#import "HTTPClient.h"
#import "Constants.h"

@implementation HTTPClient

#pragma mark - sharedHTTPClient

+ (HTTPClient *)sharedHTTPClient
{
    static HTTPClient *sharedHttpClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedHttpClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:kAPIURL]];
    });
    return sharedHttpClient;
}

#pragma mark - getResponseFromAPI:

- (void)getResponseFromAPI:(NSString *)api andParameters:(NSDictionary *)parameters
{
    [self POST:api parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress)
    {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        if ([self.delegate respondsToSelector:@selector(httpClient:didUpdateWithResponse:)])
        {
            [self.delegate httpClient:self didUpdateWithResponse:responseObject];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
    {
        if ([self.delegate respondsToSelector:@selector(httpClient:didFailedWithError:)])
        {
            [self.delegate httpClient:self didFailedWithError:error];
        }
    }];
}


@end
